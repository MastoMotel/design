```
This file is currently the brainstorming area / design doc. If you want to
contribute, you can do it by issuing a pull request. Or sending me patches.
Or just complain in issues.
```

# Masto Motel
Masto Motel is a game based on the same principles as the Mastodon social
network.
It is built around the idea of instances being small worlds that users can visit
and socialize in, it is also a slightly over-the-top interface for Mastodon
itself.

# Theme
The vague overarching theme for the first implementation will be "roadtripping
between motels" but other kinds of worlds are also anticipated, such as forests
or medieval witches' huts or high tech cyber cafes. The possibilities are
endless.

# Graphics
The graphics will be low-poly combined with pixel art. Assets should be easily
customizable by users with no knowledge of 3D. 3D was chosen because it makes
richer interactions possible and is easy to generate animations for, whereas
2D art needs hand drawn animations.

## Perspectives, controls
It should be playable from a WASD+mouse based FPS/TPS mode and a purely mouse
based overview mode. This makes the game more accessible and will make porting
to touchscreen easier.

# Implementation
## Language
Will likely use Rust, as the game should be performant and cross platform, while
also being safe. We don't want any users' machines to be compromised because of
our game. Online societies are often targets of griefing/trolling, so security
of data and resistance to other forms of attacks should be an important design
goal.

## Engine
I'm considering Amethyst but since it doesn't look ready, and to be honest, none
of the Rust engines do, it might be better to just rool our own. Not a general
purpose one, just something that works for _this_ game. That doesn't mean we
have to do everything from scratch, we can utilize existing Rust crates that are
engine independent.

## Platforms
At first it should run on Linux, BSD, Mac and Windows, then we can worry about
mobiles and browsers. Graphics should target OpenGL ES 2.0 or 2.1, since it's
widely supported and would make a WebGL port easy. It shouldn't use complex
shaders.

# User generated content
Assets should be federated, so users can use assets from other instances.
Users should definitely be able to create avatars and cosmetic changes.
Furniture and other objects are next. These should be easily parametrized, eg.
over what texture they use or what sound effects.

User contributed scripts... not sure. Probably no-no at first, if we do it then
we could use Lua or some Scheme dialect. It should be easily sandboxed.

At first we should prefer config files over scripts. What config format to use
is still undecided. If we are going to use Lua anyways then it could just be
that.

# Scripted events
Hell yes! But these do not federate like assets do, they are instance specific.
This means we need scripting after all, but these don't necessarily need to be
so deeply sandboxed. Eg.: we should let admins use binary Lua modules.

# Moderation
Base it on Mastodon for now.