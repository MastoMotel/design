Start out by prototyping the client and building the server backend as we go.
Do the same on the server, ie.: start building it to fit the client and add the
federation features as we go.

We should have a "walking around in a room" demo first, then a basic room
editor demo, then we add federation to the whole thing.

HOWEVER! we should read the ActivityPub spec BEFORE any server stuff starts!
This is important because I want to know how we will handle user identities and
such. I'm not 100% sure it's necessary but I don't want to risk having to redo
things like login.